from sys import argv
import boto3
import time
import os


def main():
    VERSION = f'{os.getenv("CI_COMMIT_REF_NAME")}-{os.getenv("CI_PIPELINE_ID")}'
    region = 'ap-southeast-1'
    ssm_filter = [{'Key': 'tag:SSM', 'Values': [f'netjs-app-{argv[1]}']}]
    boto3.setup_default_session(profile_name='mfa')
    ssm = boto3.client('ssm', region_name=region)
    instance_info = ssm.describe_instance_information(Filters=ssm_filter).get('InstanceInformationList', {})[0]

    instance_id = instance_info.get('InstanceId', '')
    cmd1 = 'cd /opt/devops'
    cmd2 = f"sed -i 's/export NETJS_BE_VERSION=.*/export NETJS_BE_VERSION={VERSION}/g' /opt/devops/version.txt"
    cmd3 = 'aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 107858015234.dkr.ecr.ap-southeast-1.amazonaws.com'
    cmd4 = 'source /opt/devops/version.txt; /usr/local/bin/docker-compose up -d netjs-be'
    cmds = [cmd1, cmd2, cmd3, cmd4]
    response = ssm.send_command(InstanceIds=[instance_id],
                                DocumentName='AWS-RunShellScript',
                                TimeoutSeconds=60,
                                Parameters={"commands": cmds,
                                            "executionTimeout": ['900']}
                                )
    command_id = response.get('Command', {}).get("CommandId", None)
    invocation = None
    while True:
        response = ssm.list_command_invocations(
            CommandId=command_id, Details=True)
        """ If the command hasn't started to run yet, keep waiting """
        if len(response['CommandInvocations']) == 0:
            time.sleep(1)
            continue
        invocation = response['CommandInvocations'][0]
        if invocation['Status'] in ('Success', 'TimedOut', 'Cancelled', 'Failed'):
            break
        else:
            time.sleep(60)
    command_plugin = invocation['CommandPlugins'][-1]
    output = command_plugin['Output']
    status = command_plugin['Status']
    if status != 'Success':
        print(
            f"Failed to deploy NETJS_BE_VERSION {VERSION}, status {status}, output: {output}")
    else:
        print(f"Complete deploy NETJS_BE_VERSION {VERSION}")


if __name__ == "__main__":
    main()
