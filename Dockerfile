FROM node:16-alpine

WORKDIR /usr/src/app

RUN chown -R node:node /usr/src/app

COPY --chown=node package*.json ./

USER node

RUN npm install

COPY --chown=node . .

RUN npm run build

CMD ["node", "dist/main"]